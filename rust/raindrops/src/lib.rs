/// - If the number has 3 as a factor, output 'Pling'.
/// - If the number has 5 as a factor, output 'Plang'.
/// - If the number has 7 as a factor, output 'Plong'.
/// - If the number does not have 3, 5, or 7 as a factor,
///   just pass the number's digits straight through.
pub fn raindrops(n: u32) -> String {
    let mut response: String = String::default();
    if n % 3 == 0 {
        response += "Pling";
    }
    if n % 5 == 0 {
        response += "Plang";
    }
    if n % 7 == 0 {
        response += "Plong";
    }
    if response == "" {
        n.to_string()
    } else {
        response
    }
}
