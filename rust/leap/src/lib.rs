/// * on every year that is evenly divisible by 4
///   * except every year that is evenly divisible by 100
///     * unless the year is also evenly divisible by 400
pub fn is_leap_year(year: u64) -> bool {
    is_factor_of(400, year) || (is_factor_of(4, year) && !is_factor_of(100, year))
}

fn is_factor_of(factor: u64, value: u64) -> bool {
    0 == value % factor
}
