# Exercism Exercises

This repository is for me to track my progress through various exercism tracks.

I will generally commit uncompleted tracks to this repository; to see my progress visit `config.yml` or the CI/CD
pipelines.

The MIT Licence covers everything other than the code provided in the exercism tracks.
The exercism tracks generally contain: README, config files, tests.
